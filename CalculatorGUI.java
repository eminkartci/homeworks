
// I will use swing and awt
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class CalculatorGUI {

    public static void main(String[] args) {
        
        // create instance
        CalculatorGUI myCalc = new CalculatorGUI();

    }


    // Constants
	private String[] BUTTON_NAMES = {"7","8","9","/","4","5","6","x","1","2","3","-","0",".","=","+"};
	private Color[] BUTTON_COLORS = {	Color.WHITE,Color.WHITE,Color.WHITE,Color.LIGHT_GRAY,
										Color.WHITE,Color.WHITE,Color.WHITE,Color.LIGHT_GRAY,
										Color.WHITE,Color.WHITE,Color.WHITE,Color.LIGHT_GRAY,
										Color.WHITE,Color.WHITE,Color.LIGHT_GRAY,Color.LIGHT_GRAY };
	
    // Attributes
    private JFrame jframe;
	
	private JPanel resultPanel;
	private JPanel mainPanel;
	
	private JButton[] calculatorButtons;
	
	private Label resultLabel;

    // Constructor
    public CalculatorGUI() {
		
		initGUI();
		
	}
	
	

    // Behavours
    private void initGUI() {

        // initialize and set frame
        this.jframe = new JFrame("Calculator");
		this.jframe.setSize(new Dimension(300,300));
		this.jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // initialize the frame
            // set flowLayout Right
        this.resultPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        // set color white
        this.resultPanel.setBackground(Color.WHITE);

        // Create lable
		this.resultLabel = new Label("0");
        this.resultLabel.setFont(new Font("Courier", Font.PLAIN, 20));

        // Initialize key Panel
		this.mainPanel = new JPanel();

        // Make layout Grid 4x4
		GridLayout keyGridLayout = new GridLayout(4,4);
		this.mainPanel.setLayout(keyGridLayout); // Set layout

        // Initialize buttons array
        this.calculatorButtons = new JButton[16];

        // Create all buttons
		for(int i = 0 ; i < 16 ; i++) {
            // Create - assign button
			this.calculatorButtons[i] = new JButton(BUTTON_NAMES[i]);
            // Change background color
			this.calculatorButtons[i].setBackground(BUTTON_COLORS[i]);
            // Add to mainPanel
			this.mainPanel.add(this.calculatorButtons[i]);			
		}
		
        // Add label and mainPanel to the result panel
		this.resultPanel.add(this.resultLabel);
		this.resultPanel.add(this.mainPanel);
		
        // Set their location on the jframe
		this.jframe.getContentPane().add(BorderLayout.NORTH,resultPanel);
		this.jframe.getContentPane().add(BorderLayout.CENTER,mainPanel);


        // make frame visible
        this.jframe.setVisible(true);

    }

    
}
