import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Dimension;

public class TripleWithRadioButtons { public static void main(String[] args) {

    // Given Part Start
    JFrame app = new JFrame();
    app.add(new TripleWithRadioButtonsPanel()); 
    app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
    app.setSize(200, 200);
    app.setMinimumSize(new Dimension(200,200));
    app.setMaximumSize(new Dimension(200,200));
    app.setVisible(true);
    } 
    // Given Part End
}

class TripleWithRadioButtonsPanel extends JPanel{

    // Extra Attributes
    private JTextField textField;
    private Label label;
    private JButton appendButton, resetButton;
    private JRadioButton lowercaseRadioBtn, uppercaseRadioBtn;
    private JPanel[] intervPanels;


    // Constructor
    public TripleWithRadioButtonsPanel(){

        initComponents();

    }

    // Behaviours

    private void initComponents(){

        // create the instances
        this.textField          = new JTextField(15);
        this.label              = new Label("Hi.");
        this.label.setFont(new Font("Courier", Font.BOLD, 12));

        this.resetButton        = new JButton("Reset");
        this.appendButton       = new JButton("Append");

        this.uppercaseRadioBtn = new JRadioButton("UPPERCASE");
        this.lowercaseRadioBtn = new JRadioButton("lowercase");

        // button group
        ButtonGroup radioGroup = new ButtonGroup(); 
        radioGroup.add(this.uppercaseRadioBtn);
        radioGroup.add(this.lowercaseRadioBtn);

        // Layout
        GridLayout layout = new GridLayout(6,1);
        this.setLayout(layout);

        // Center the items flowlayout
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);

        this.intervPanels = new JPanel[5];

        for(int i = 0 ; i < this.intervPanels.length ; i++){
            this.intervPanels[i] = new JPanel(new FlowLayout(FlowLayout.CENTER));
            this.add( this.intervPanels[i]);
        }

        

        // Add the components
        this.intervPanels[0].add(this.label);
        this.intervPanels[1].add(this.textField);
        this.intervPanels[2].add(this.appendButton);
        this.intervPanels[2].add(this.resetButton);
        this.intervPanels[3].add(this.lowercaseRadioBtn);
        this.intervPanels[4].add(this.uppercaseRadioBtn);


    }



}